﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidad
{
    public abstract class Identificador
    {
        public string Id { get; set; }
    }
}
