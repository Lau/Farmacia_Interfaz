﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidad
{
    public class PersonaClass:Identificador
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public string telefono { get; set; }
        public string Correo { get; set; }
    }
}
