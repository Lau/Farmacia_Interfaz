﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidad
{
    public class CategoriaClass : Identificador
    {
        public string CategoriaProducto { get; set; }
        public override string ToString ()
        {
            return CategoriaProducto;  
        }
    }
}
    