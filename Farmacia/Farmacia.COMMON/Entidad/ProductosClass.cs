﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidad
{
    public class ProductosClass: Identificador
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string PrecioCompra { get; set; }
        public string PrecioVenta { get; set; }
        public string Presentacion { get; set; }
        public string ProductoCategoria { get; set; }
        public string Stock { get; set; }
    }
}
