﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidad
{
    public class EmpleadoClass: PersonaClass
    {
        public string Sueldo { get; set; }
        public string Horario { get; set; }
        public override string ToString()
        {
            return Nombre + " "+ Apellido;
        }
    }
}
