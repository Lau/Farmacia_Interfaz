﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Entidad
{
    public class VentaClass2
    {
        public string Producto { get; set; }
        public float Precio { get; set; }
        public int Cantidad { get; set; }
        public float Total { get; set; }
    }
}
