﻿using Farmacia.COMMON.Entidad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Interfaz
{
    public interface IManejadorGenerico<T> where T:Identificador
    {
        bool Agregar(T entidad);
        List<T> Lista { get; }
        bool Eliminar(string id);
        bool Modificar(T entidad);
        T BuscarIdentificador(string id);//es hacer una consulta
    }
}
