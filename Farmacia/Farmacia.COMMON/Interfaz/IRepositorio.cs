﻿using Farmacia.COMMON.Entidad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Interfaz
{
    public interface IRepositorio<T> where T:Identificador//es una interfaz generica
    {//t es tipo
        bool Crear(T entidad);
        bool Editar(T entidadModificada);
        bool Eliminar(string id);
        List<T> Leer { get; }
    }
}
