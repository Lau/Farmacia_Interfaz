﻿using Farmacia.COMMON.Entidad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.COMMON.Interfaz
{
    public interface IManejadorProductos:IManejadorGenerico<ProductosClass>
    {
        List<ProductosClass> ProductosPorCategoria(string categoria);
    }
}
