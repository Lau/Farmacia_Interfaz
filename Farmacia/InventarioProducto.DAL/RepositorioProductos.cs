﻿using Farmacia.COMMON.Entidad;
using Farmacia.COMMON.Interfaz;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InventarioProducto.DAL
{
    public class RepositorioProductos : IRepositorio<ProductosClass>
    {
        private string DBName = "Farmacia.db";//nombre de la base de datos
        private string TableName = "Productos";//nombre de la tabla
        public List<ProductosClass> Leer {
            get {
                List<ProductosClass> datos = new List<ProductosClass>();
                using (var db = new LiteDatabase(DBName))
                {//retiene un elemento
                    datos = db.GetCollection<ProductosClass>(TableName).FindAll().ToList();

                }
                return datos;
            }
        }

        public bool Crear(ProductosClass entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();//este genera un numero aleatorio unico
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<ProductosClass>(TableName);
                    coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(ProductosClass entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<ProductosClass>(TableName);
                    coleccion.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<ProductosClass>(TableName);
                    r = coleccion.Delete(e => e.Id == id); //expresion landa cambia deun foreach//especifique para encontar los elementos a eliminar
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
