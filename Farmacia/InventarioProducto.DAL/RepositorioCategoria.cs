﻿using Farmacia.COMMON.Entidad;
using Farmacia.COMMON.Interfaz;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InventarioProducto.DAL
{
    public class RepositorioCategoria :IRepositorio<CategoriaClass>
    {
        private string DBName = "Farmacia.db";//nombre de la base de datos
        private string TableName = "Categoria";//nombre de la tabla

        public List<CategoriaClass> Leer
        {
            get
            {
                List<CategoriaClass> categoria = new List<CategoriaClass>();
                using (var db = new LiteDatabase(DBName))
                {//retiene un elemento
                    categoria = db.GetCollection<CategoriaClass>(TableName).FindAll().ToList();

                }
                return categoria;
            }
        }

        public bool Crear(CategoriaClass entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();//este genera un numero aleatorio unico
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<CategoriaClass>(TableName);
                    coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(CategoriaClass entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<CategoriaClass>(TableName);
                    coleccion.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<CategoriaClass>(TableName);
                    r = coleccion.Delete(e => e.Id == id); //expresion landa cambia deun foreach//especifique para encontar los elementos a eliminar
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
