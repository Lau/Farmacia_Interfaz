﻿using Farmacia.COMMON.Entidad;
using Farmacia.COMMON.Interfaz;
using System;
//using LiteDB;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using LiteDB;

namespace InventarioProducto.DAL
{
    public class RepositorioEmpleado : IRepositorio<EmpleadoClass>
    {
        private string DBName = "Farmacia.db";//nombre de la base de datos
        private string TableName = "Empleados";//nombre de la tabla

        public List<EmpleadoClass> Leer
        {
            get
            {
                List<EmpleadoClass> empleado = new List<EmpleadoClass>();
                using (var db = new LiteDatabase(DBName))
                {//retiene un elemento
                    empleado = db.GetCollection<EmpleadoClass>(TableName).FindAll().ToList();

                }
                return empleado;
            }
        }

        public bool Crear(EmpleadoClass entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();//este genera un numero aleatorio unico
            try
            {
                using (var db = new LiteDatabase(DBName)) {
                    var coleccion = db.GetCollection<EmpleadoClass>(TableName);
                    coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(EmpleadoClass entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<EmpleadoClass>(TableName);
                    coleccion.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<EmpleadoClass>(TableName);
                    r = coleccion.Delete(e => e.Id==id ); //expresion landa cambia deun foreach//especifique para encontar los elementos a eliminar
                }
                return r>0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}