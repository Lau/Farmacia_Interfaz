﻿using Farmacia.COMMON.Entidad;
using Farmacia.COMMON.Interfaz;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InventarioProducto.DAL
{
    public class RepositorioDeVentas : IRepositorio<InventarioVentas>
    {
        private string DBName = "Farmacia.db";//nombre de la base de adtos
        private string TableName = "InventarioDeVentas";//nombre de la tabla
        public List<InventarioVentas> Leer {
            get {
                List<InventarioVentas> inventario = new List<InventarioVentas>();
                using (var db = new LiteDatabase(DBName)) {//retiene el elemento
                    inventario = db.GetCollection<InventarioVentas>(TableName).FindAll().ToList();
                }
                return inventario;
            }
        }
       
        public bool Crear(InventarioVentas entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();//este genera un numero aleatorio unico
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<InventarioVentas>(TableName);
                    coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Editar(InventarioVentas entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<InventarioVentas>(TableName);
                    coleccion.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Eliminar(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<InventarioVentas>(TableName);
                    r = coleccion.Delete(e => e.Id == id); //expresion landa cambia deun foreach//especifique para encontar los elementos a eliminar
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
