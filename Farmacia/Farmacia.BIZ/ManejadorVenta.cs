﻿using Farmacia.COMMON.Entidad;
using Farmacia.COMMON.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class ManejadorVenta : IManejadorVenta1
    {
        IRepositorio<InventarioVentas> venta;
        public ManejadorVenta(IRepositorio<InventarioVentas> venta) {
            this.venta = venta;
        }
        public List<InventarioVentas> Lista => venta.Leer;

        public bool Agregar(InventarioVentas entidad)
        {
            return venta.Crear(entidad);
        }

        public InventarioVentas BuscarIdentificador(string id)
        {
            return Lista.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return venta.Eliminar(id);
        }

        public bool Modificar(InventarioVentas entidad)
        {
            return venta.Editar(entidad);
        }
    }
}
