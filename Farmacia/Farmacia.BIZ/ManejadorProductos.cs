﻿using Farmacia.COMMON.Entidad;
using Farmacia.COMMON.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class ManejadorProductos : IManejadorProductos
    {
        IRepositorio<ProductosClass> productos;
        public ManejadorProductos(IRepositorio<ProductosClass> pro) {
            this.productos = pro;
        }
        public List<ProductosClass> Lista => productos.Leer;

        public bool Agregar(ProductosClass entidad)
        {
            return productos.Crear(entidad);
        }

        public ProductosClass BuscarIdentificador(string id)
        {
            return Lista.Where(e=> e.Nombre==id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return productos.Eliminar(id);
        }

        public bool Modificar(ProductosClass entidad)
        {
            return productos.Editar(entidad);
        }

        public List<ProductosClass> ProductosPorCategoria(string categoria)
        {
            return Lista.Where(e => e.ProductoCategoria == categoria).ToList();
        }
    }
}
