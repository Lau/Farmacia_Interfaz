﻿using Farmacia.COMMON.Entidad;
using Farmacia.COMMON.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class ManejadorCategoria : IManejadorCategoria
    {
        IRepositorio<CategoriaClass> categoria;
        public ManejadorCategoria(IRepositorio<CategoriaClass> categoria) {
            this.categoria = categoria;
        }

        public List<CategoriaClass> Lista => categoria.Leer;

        public bool Agregar(CategoriaClass entidad)
        {
            return categoria.Crear(entidad);
        }

        public CategoriaClass BuscarIdentificador(string id)
        {
            return Lista.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return categoria.Eliminar(id);
        }

        public bool Modificar(CategoriaClass entidad)
        {
            return categoria.Editar(entidad);
        }
    }
}
