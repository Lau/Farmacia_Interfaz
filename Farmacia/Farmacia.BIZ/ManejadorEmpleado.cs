﻿using Farmacia.COMMON.Entidad;
using Farmacia.COMMON.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class ManejadorEmpleado : IManejadorEmpleado
    {
        IRepositorio<EmpleadoClass> empleado;
        public ManejadorEmpleado(IRepositorio<EmpleadoClass> empleado) {
            this.empleado = empleado;
        }

        public List<EmpleadoClass> Lista => empleado.Leer;

        public bool Agregar(EmpleadoClass entidad)
        {
            return empleado.Crear(entidad);
        }

        public EmpleadoClass BuscarIdentificador(string id)
        {
            return Lista.Where(e=> e.Id==id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return empleado.Eliminar(id);
        }

        public bool Modificar(EmpleadoClass entidad)
        {
            return empleado.Editar(entidad);
        }
    }
}
