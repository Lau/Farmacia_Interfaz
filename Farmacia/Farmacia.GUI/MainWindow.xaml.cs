﻿using Farmacia.BIZ;
using Farmacia.COMMON.Archivo;
using Farmacia.COMMON.Entidad;
using Farmacia.COMMON.Interfaz;
using InventarioProducto.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Farmacia.GUI
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool N;
        List<VentaClass2> venta;
        float iva = 0.16f;
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorProductos manejadorProductos;
        IManejadorCategoria manejadorCategoria;
        IManejadorEmpleado manejadorEmpleado;
        IManejadorVenta1 manejadorVenta;
        accion acc;

        public MainWindow()
        {
            InitializeComponent();
            manejadorProductos = new ManejadorProductos(new RepositorioProductos());
            manejadorCategoria = new ManejadorCategoria(new RepositorioCategoria());
            manejadorEmpleado = new ManejadorEmpleado(new RepositorioEmpleado());
            manejadorVenta = new ManejadorVenta(new RepositorioDeVentas()); 
            venta = new List<VentaClass2>();
            AccionesParaProductos();
            AccionesParaCategoria();
            AccionesParaEmpleado();
            AccionesAlmacenDeVentanas();
            CargarFormularioVenta();//formulario de la venta
        }

        private void AccionesAlmacenDeVentanas() {
            ActualizarTablaInvenatarioDeVenta();
            LimpiarBotonesEnAlmacen();
        }

        private void ActualizarTablaInvenatarioDeVenta() {
            dtgAlmacenDeVentaInventario.ItemsSource = null;
            dtgAlmacenDeVentaInventario.ItemsSource = manejadorVenta.Lista;
        }

        private void ActualizarTablaVentaAlmacen() {
            dtgProductosVentaAlmacen.ItemsSource = null;
            dtgProductosVentaAlmacen.ItemsSource = manejadorProductos.Lista;
        }

        private void CargarFormularioVenta() {
            txtVentaFecha.Text = DateTime.Now.ToShortDateString();
            ActualizarTablaEmpleado();
            ActualizarComboxDeTodosLosFormularios();
            ActualizarTablaVentaAlmacen();
            ActualizarTablaProductosEnVenta(); // tabla de la de venta
            InabilitarCamposVenta(false);// los botones de la venta
        }

        private void ActualizarComboxDeTodosLosFormularios() {
            manejadorCategoria = new ManejadorCategoria(new RepositorioCategoria());
            manejadorEmpleado = new ManejadorEmpleado(new RepositorioEmpleado());
            ProductosCmbCategoria.ItemsSource = manejadorCategoria.Lista;//carga de combobox
            cmbVentaNombreEmpleado.ItemsSource = manejadorEmpleado.Lista;
        }

        private void AccionesParaEmpleado()
        {
            LimpiarEmpleado(false);
            BotonesEmpleado(false);
            ActualizarTablaEmpleado();
        }

        private void BotonesEmpleado(bool v)
        {
            EmpleadoBtnCancelar.IsEnabled = v;
            EmpleadoBtnEliminar.IsEnabled = !v;
            EmpleadoBtnGuardar.IsEnabled = v;
            EmpleadoBtnNuevo.IsEnabled = !v;
            EmpleadoBtnEditar.IsEnabled = !v;
        }

        private void ActualizarTablaEmpleado()
        {
            dtgEmpleado.ItemsSource = null;
            dtgEmpleado.ItemsSource = manejadorEmpleado.Lista;
        }

        private void LimpiarEmpleado(bool v)
        {
            TxbEmpleadoTelefono.Clear();
            TxbEmpleadoSueldo.Clear();
            TxbEmpleadoNombre.Clear();
            TxbEmpleadoHorario.Clear();
            TxbEmpleadoDireccion.Clear();
            TxbEmpleadoCorreo.Clear();
            TxbEmpleadoApellido.Clear();
            TxbCategoriaNombre.Clear();
            TxbEmpleadoTelefono.IsEnabled = v;
            TxbEmpleadoSueldo.IsEnabled = v;
            TxbEmpleadoNombre.IsEnabled = v;
            TxbEmpleadoHorario.IsEnabled = v;
            TxbEmpleadoDireccion.IsEnabled = v;
            TxbEmpleadoCorreo.IsEnabled = v;
            TxbEmpleadoApellido.IsEnabled = v;
            TxbCategoriaNombre.IsEnabled = v;
        }

        private void AccionesParaProductos() {
            BotonesProductos(false);
            LimpiarProductos(false);
            ActualizarTablaProductos();
            ActualizarTablaVentaAlmacen();
        }

        private void AccionesParaCategoria() {
            BotonesCategoria(false);
            LimpiarCategoria(false);
            ActualizarTablaCategoria();
            ActualizarComboxDeTodosLosFormularios();
        }

        private void ActualizarTablaCategoria()
        {
            dtgCategoria.ItemsSource = null;
            dtgCategoria.ItemsSource = manejadorCategoria.Lista;
        }

        private void LimpiarCategoria(bool v)
        {
            TxbCategoriaNombre.Clear();
            TxbCategoriaNombre.IsEnabled = v;
        }

        private void BotonesCategoria(bool v)
        {
            CategoriaBtnCancelar.IsEnabled = v;
            CategoriaBtnEditar.IsEnabled = !v;
            CategoriaBtnEliminar.IsEnabled = !v;
            CategoriaBtnGuardar.IsEnabled = v;
            CategoriaBtnNuevo.IsEnabled = !v;
        }

        private void ActualizarTablaProductos()
        {
            dtgProductos.ItemsSource = null;
            dtgProductos.ItemsSource = manejadorProductos.Lista;
        }

        private void LimpiarProductos(bool v)
        {
            ProductosTxbDescripcion.Clear();
            ProductosTxbNombre.Clear();
            ProductosTxbPrecioCompra.Clear();
            ProductosTxbPrecioVenta.Clear();
            ProductosTxbPresentacion.Clear();
            ProductosTxbStock.Clear();
            // ProductosCmbCategoria.ClearValue();
            ProductosTxbStock.IsEnabled = v;
            ProductosTxbPresentacion.IsEnabled = v;
            ProductosTxbPrecioVenta.IsEnabled = v;
            ProductosTxbPrecioCompra.IsEnabled = v;
            ProductosTxbNombre.IsEnabled = v;
            ProductosTxbDescripcion.IsEnabled = v;
            ProductosCmbCategoria.IsEnabled = v;

        }

        private void BotonesProductos(bool v)
        {
            ProductosBtnCancelar.IsEnabled = v;
            ProductosBtnEditar.IsEnabled = !v;
            ProductosBtnEliminar.IsEnabled = !v;
            ProductosBtnGuardar.IsEnabled = v;
            ProductosBtnNuevo.IsEnabled = !v;
        }

        private void ProductosBtnNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarProductos(true);
            BotonesProductos(true);
            acc = accion.Nuevo;
        }

        private void ProductosBtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(ProductosTxbNombre.Text)) {
                MessageBox.Show("No ha llenado el campo Nombre", "Productos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(ProductosTxbDescripcion.Text))
            {
                MessageBox.Show("No ha llenado el campo Descripción", "Productos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(ProductosTxbPrecioCompra.Text))
            {
                MessageBox.Show("No ha llenado el campo Precio de Compra", "Productos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            /*Para checar si son numeros*/
            foreach (var item in ProductosTxbPrecioCompra.Text)
            {
                if (!(char.IsNumber(item)))
                {
                    MessageBox.Show("Solo se permiten números en el Precio Compra, no caracteres", "Productos", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            if (string.IsNullOrEmpty(ProductosTxbPrecioVenta.Text))
            {
                MessageBox.Show("No ha llenado el campo Precio de venta", "Productos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            /*Para checar si son numeros*/
            foreach (var item in ProductosTxbPrecioVenta.Text)
            {
                if (!(char.IsNumber(item)))
                {
                    MessageBox.Show("Solo se permiten números en Precio Venta, no caracteres", "Ventas", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            if (string.IsNullOrEmpty(ProductosTxbPresentacion.Text))
            {
                MessageBox.Show("No ha llenado el campo Presentación", "Productos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(ProductosTxbStock.Text))
            {
                MessageBox.Show("No ha llenado el campo Stock", "Productos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            /*Para checar si son numeros*/
            foreach (var item in ProductosTxbStock.Text)
            {
                if (!(char.IsNumber(item)))
                {
                    MessageBox.Show("Solo se permiten números en Stock, no caracteres", "Ventas", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            if (string.IsNullOrEmpty(ProductosCmbCategoria.Text))
            {
                MessageBox.Show("No ha llenado el campo Categoria", "Productos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (acc == accion.Nuevo)
            {
                ProductosClass productos = new ProductosClass()
                {
                    Nombre = (ProductosTxbNombre.Text).ToUpper(),
                    Descripcion = (ProductosTxbDescripcion.Text).ToUpper(),
                    PrecioCompra = ProductosTxbPrecioCompra.Text,
                    PrecioVenta = ProductosTxbPrecioVenta.Text,
                    Presentacion = ProductosTxbPresentacion.Text.ToUpper(),
                    Stock = ProductosTxbStock.Text,
                    ProductoCategoria = (ProductosCmbCategoria.Text).ToString()
                };
                if (manejadorProductos.Agregar(productos))
                {
                    MessageBox.Show("Producto agregado correctamente", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                    AccionesParaProductos();
                }
                else
                {
                    MessageBox.Show("El Producto no se pudo agregar", "Producto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                ProductosClass productos = dtgProductos.SelectedItem as ProductosClass;
                productos.Nombre = ProductosTxbNombre.Text;
                productos.PrecioCompra = ProductosTxbPrecioCompra.Text;
                productos.PrecioVenta = ProductosTxbPrecioVenta.Text;
                productos.Presentacion = ProductosTxbPresentacion.Text;
                productos.Stock = ProductosTxbStock.Text;
                productos.ProductoCategoria = ProductosCmbCategoria.Text.ToString();
                if (manejadorProductos.Modificar(productos))
                {
                    MessageBox.Show("Producto modificado correctamente", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                    AccionesParaProductos();
                }
                else
                {
                    MessageBox.Show("El Producto no se pudo actualizar", "Producto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ProductosBtnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (manejadorProductos.Lista.Count == 0) {
                MessageBox.Show("No cuenta con ningun producto para editar", "Producto", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            if (dtgProductos.SelectedItem != null)
            {
                try
                {

                    ProductosClass productos = dtgProductos.SelectedItem as ProductosClass;
                    LimpiarProductos(true);
                    ProductosTxbDescripcion.Text = productos.Descripcion;
                    ProductosTxbNombre.Text = productos.Nombre;
                    ProductosTxbPrecioCompra.Text = productos.PrecioCompra;
                    ProductosTxbPrecioVenta.Text = productos.PrecioVenta;
                    ProductosTxbPresentacion.Text = productos.Presentacion;
                    ProductosTxbStock.Text = productos.Stock;
                    ProductosCmbCategoria.Text = productos.ProductoCategoria;
                    acc = accion.Editar;
                    BotonesProductos(true);
                }
                catch (Exception)
                {

                    MessageBox.Show("No se puedo realizar la operacion", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else {
                MessageBox.Show("No ha seleccionado ningun producto para modificar", "Producto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ProductosBtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Esta seguro de cancelar la operación", "Productos", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
            {
                AccionesParaProductos();
            }
        }

        private void ProductosBtnEliminar_Click(object sender, RoutedEventArgs e)
        {
            ProductosClass productos = dtgProductos.SelectedItem as ProductosClass;
            if (productos != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este producto?", "Producto", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorProductos.Eliminar(productos.Id))
                    {
                        MessageBox.Show("Producto eliminado", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaProductos();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Producto", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            else {
                MessageBox.Show("No ha seleccionado ningun Producto", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void CategoriaBtnNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCategoria(true);
            BotonesCategoria(true);
            acc = accion.Nuevo;
        }

        private void CategoriaBtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TxbCategoriaNombre.Text)) {
                MessageBox.Show("No ha agregado el campo categoria", "Categoria", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (acc == accion.Nuevo) {
                CategoriaClass categoria = new CategoriaClass();
                categoria.CategoriaProducto = (TxbCategoriaNombre.Text).ToUpper();
                if (manejadorCategoria.Agregar(categoria))
                {
                    MessageBox.Show("Categoria agregado correctamente", "Categoria", MessageBoxButton.OK, MessageBoxImage.Information);
                    AccionesParaCategoria();
                }
                else {
                    MessageBox.Show("La Categoria no se pudo agregar ", "Categoria", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            } else {
                CategoriaClass categoria = dtgCategoria.SelectedItem as CategoriaClass;
                categoria.CategoriaProducto = TxbCategoriaNombre.Text.ToUpper();
                if (manejadorCategoria.Modificar(categoria)) {
                    MessageBox.Show("Categoria editada correctamente", "Categoria", MessageBoxButton.OK, MessageBoxImage.Information);
                    AccionesParaCategoria();
                } else {
                    MessageBox.Show("La Categoria no se pudo modificar ", "Categoria", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void CategoriaBtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Esta seguro de cancelar la operación", "Categoria", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes) {
                AccionesParaCategoria();
            }
        }

        private void CategoriaBtnEditar_Click(object sender, RoutedEventArgs e)
        {
            CategoriaClass categoria = dtgCategoria.SelectedItem as CategoriaClass;
            if (categoria != null)
            {
                LimpiarCategoria(true);
                TxbCategoriaNombre.Text = categoria.CategoriaProducto;
                acc = accion.Editar;
                BotonesCategoria(true);
            }
            else {
                MessageBox.Show("No ha seleccionado ninguna categoria", "Categoria", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void CategoriaBtnEliminar_Click(object sender, RoutedEventArgs e)
        {
            CategoriaClass categoria = dtgCategoria.SelectedItem as CategoriaClass;
            if (categoria != null) {
                if (MessageBox.Show("Realmente esta seguro de eliminar esta categoria", "Categoria", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes) {
                    if (manejadorCategoria.Eliminar(categoria.Id)) {
                        MessageBox.Show("Categoria eliminada", "Categoria", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaCategoria();
                        ActualizarComboxDeTodosLosFormularios();
                    }
                    else {
                        MessageBox.Show("Realmente esta seguro de eliminar esta categoria", "Categoria", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            } else
            {
                MessageBox.Show("No ha seleccionado ninguna categoria", "Categoria", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void EmpleadoBtnNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarEmpleado(true);
            BotonesEmpleado(true);
            acc = accion.Nuevo;
        }

        private void EmpleadoBtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TxbEmpleadoApellido.Text)) {
                MessageBox.Show("Falta ingresar en Apellido", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(TxbEmpleadoCorreo.Text))
            {
                MessageBox.Show("Falta ingresar el correo", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(TxbEmpleadoDireccion.Text))
            {
                MessageBox.Show("Falta ingresar la Dirección", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(TxbEmpleadoHorario.Text))
            {
                MessageBox.Show("Falta ingresar el horario", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(TxbEmpleadoNombre.Text))
            {
                MessageBox.Show("Falta ingresar el Nombre", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(TxbEmpleadoSueldo.Text))
            {
                MessageBox.Show("Falta ingresar el Sueldo", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            /*Para checar si son numeros*/
            foreach (var item in TxbEmpleadoSueldo.Text)
            {
                if (!(char.IsNumber(item)))
                {
                    MessageBox.Show("Solo se permiten números en el Sueldo del empleado, no caracteres", "Ventas", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            if (string.IsNullOrEmpty(TxbEmpleadoTelefono.Text))
            {
                MessageBox.Show("Falta ingresar el Telefono", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            /*Para checar si son numeros*/
            foreach (var item in TxbEmpleadoTelefono.Text)
            {
                if (!(char.IsNumber(item)))
                {
                    MessageBox.Show("Solo se permiten números en el campo de Telefono", "Ventas", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            if (acc == accion.Nuevo)
            {
                EmpleadoClass empleado = new EmpleadoClass();
                empleado.Apellido = TxbEmpleadoApellido.Text.ToUpper();
                empleado.Correo = TxbEmpleadoCorreo.Text.ToUpper();
                empleado.Direccion = TxbEmpleadoDireccion.Text.ToUpper();
                empleado.Horario = TxbEmpleadoHorario.Text.ToUpper();
                empleado.Nombre = TxbEmpleadoNombre.Text.ToUpper();
                empleado.Sueldo = TxbEmpleadoSueldo.Text;
                empleado.telefono = TxbEmpleadoTelefono.Text;
                if (manejadorEmpleado.Agregar(empleado))
                {
                    MessageBox.Show("Empleado agregado correctamente", "Empleado", MessageBoxButton.OK, MessageBoxImage.Information);
                    AccionesParaEmpleado();
                }
                else
                {
                    MessageBox.Show("El empleado no se pudo agregar ", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                EmpleadoClass empleado = dtgEmpleado.SelectedItem as EmpleadoClass;
                empleado.Apellido = TxbEmpleadoApellido.Text.ToUpper();
                empleado.Correo = TxbEmpleadoCorreo.Text.ToUpper();
                empleado.Direccion = TxbEmpleadoDireccion.Text.ToUpper();
                empleado.Horario = TxbEmpleadoHorario.Text.ToUpper();
                empleado.Nombre = TxbEmpleadoNombre.Text.ToUpper();
                empleado.Sueldo = TxbEmpleadoSueldo.Text;
                empleado.telefono = TxbEmpleadoTelefono.Text;
                if (manejadorEmpleado.Modificar(empleado))
                {
                    MessageBox.Show("Empleado editada correctamente", "Empleado", MessageBoxButton.OK, MessageBoxImage.Information);
                    AccionesParaEmpleado();
                }
                else
                {
                    MessageBox.Show("El empleado no se pudo modificar ", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }

        private void EmpleadoBtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Esta seguro de cancelar la operación", "Empleado", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) {
                AccionesParaEmpleado();
            }
        }

        private void EmpleadoBtnEditar_Click(object sender, RoutedEventArgs e)
        {
            EmpleadoClass empleado = dtgEmpleado.SelectedItem as EmpleadoClass;
            if (empleado != null)
            {
                LimpiarEmpleado(true);
                TxbEmpleadoTelefono.Text = empleado.telefono;
                TxbEmpleadoSueldo.Text = empleado.Sueldo;
                TxbEmpleadoNombre.Text = empleado.Nombre;
                TxbEmpleadoHorario.Text = empleado.Horario;
                TxbEmpleadoDireccion.Text = empleado.Direccion;
                TxbEmpleadoCorreo.Text = empleado.Correo;
                TxbEmpleadoApellido.Text = empleado.Apellido;
                TxbCategoriaNombre.Text = empleado.Nombre;
                acc = accion.Editar;
                BotonesEmpleado(true);
            }
            else
            {
                MessageBox.Show("No ha seleccionado ningun empleado para modificar", "Empleado", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EmpleadoBtnEliminar_Click(object sender, RoutedEventArgs e)
        {
            EmpleadoClass empleado = dtgEmpleado.SelectedItem as EmpleadoClass;
            if (empleado != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este empleado: " + empleado.Nombre + "?", "Empleado", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorEmpleado.Eliminar(empleado.Id))
                    {
                        MessageBox.Show("Empleado eliminado", "Empleado", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaEmpleado();
                        ActualizarComboxDeTodosLosFormularios();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Empleado", "Empleado", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            else
            {
                MessageBox.Show("No ha seleccionado ningun Empleado", "Empleado", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void VentaBtnAgregar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtVentaCantidadProducto.Text))
            {
                MessageBox.Show("No ha colocado la cantidad de producto", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            /*Validacion para saber si son numeros */
            foreach (var item in txtVentaCantidadProducto.Text)
            {
                if (!(char.IsNumber(item)))
                {
                    MessageBox.Show("Solo se permiten números, no caracteres", "Ventas", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            if (int.Parse(txtVentaCantidadProducto.Text) <= 0)
            {
                MessageBox.Show("No se aceptan números inferiores a 1", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (N == false)
            {
                VentaClass2 v = new VentaClass2();
                ProductosClass p = dtgProductosVentaAlmacen.SelectedItem as ProductosClass;
                if (p == null)
                {
                    MessageBox.Show("No ha seleccionado ningun produto", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (int.Parse(txtVentaCantidadProducto.Text) > (int.Parse(p.Stock)))
                {
                    MessageBox.Show("No cuenta con suficiente Stock", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                v.Producto = p.Nombre;
                v.Precio = float.Parse(p.PrecioVenta);
                v.Cantidad = int.Parse(txtVentaCantidadProducto.Text);
                v.Total = v.Cantidad * v.Precio;
                venta.Add(v);
                ActualizarTablaProductosEnVenta();
                txtVentaCantidadProducto.Clear();
                /*Para Actualizar La tabla deProductos de la tabla/**/
                try
                {
                    p.Nombre = p.Nombre;
                    p.PrecioCompra = p.PrecioCompra;
                    p.PrecioVenta = p.PrecioVenta;
                    p.Presentacion = p.Presentacion;
                    p.Stock = ((int.Parse(p.Stock)) - v.Cantidad).ToString();
                    p.ProductoCategoria = p.ProductoCategoria;
                    manejadorProductos.Modificar(p);
                    ActualizarTablaVentaAlmacen();
                    ActualizarTablaProductos();
                }
                catch (Exception)
                {

                    MessageBox.Show("Error no se puede descontar los productos del almacen", "Ventas", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }          
            }
            else
            {
                VentaClass2 a = dtgListaDeVenta.SelectedItem as VentaClass2;
                if (a == null)
                {
                    MessageBox.Show("No ha seleccionado ningun produto", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                int evaluarStock = 0;
                foreach (var item in manejadorProductos.Lista)
                {
                    if (item.Nombre==a.Producto) {
                        if (int.Parse(item.Stock) < int.Parse(txtVentaCantidadProducto.Text))
                        {
                            MessageBox.Show("No hay suficiente Stock. Almacenamiento: " + item.Stock + " De: " + txtVentaCantidadProducto.Text, "Venta", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                           txtVentaCantidadProducto.Clear();
                            return;
                        }
                        evaluarStock = int.Parse(item.Stock) + a.Cantidad;
                    }
                }
                a.Producto = a.Producto;
                a.Cantidad = int.Parse(txtVentaCantidadProducto.Text);
                a.Precio = (a.Precio);
                a.Total = ((a.Precio)) * (int.Parse(txtVentaCantidadProducto.Text));
                ActualizarTablaProductosEnVenta();
                txtVentaCantidadProducto.Clear();
                /*Para actualizar la tabla de los productps igual*/
                try
                {
                    foreach (var item in manejadorProductos.Lista)
                    {
                        if (item.Nombre == a.Producto)
                        {
                            item.Nombre = item.Nombre;
                            item.PrecioCompra = item.PrecioCompra;
                            item.PrecioVenta = item.PrecioVenta;
                            item.Presentacion = item.Presentacion;
                            item.Stock = (evaluarStock - a.Cantidad).ToString();
                            item.ProductoCategoria = item.ProductoCategoria;
                            manejadorProductos.Modificar(item);
                            ActualizarTablaVentaAlmacen();
                            ActualizarTablaProductos();
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("No se puedo Editar correctamente el producto en Almacen", "Almacen", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                /*Fin de la actulizacion*/
                N = false;
            }
        }

        private void ActualizarTablaProductosEnVenta()
        {
            dtgListaDeVenta.ItemsSource = null;
            dtgListaDeVenta.ItemsSource = venta;
        }

        private void VentaBtnCalcular_Click(object sender, RoutedEventArgs e)//object sender, RoutedEventArgs e
        {
            if (venta.Count == 0) {
                MessageBox.Show("No cuenta con ningun producto en la lista para calcular", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            float total = 0;
            foreach (VentaClass2 item in venta)
            {
                total += item.Total;
            }
            txtVentaSubtotal.Text = total.ToString();
            txtVentaIva.Text = (iva * total).ToString();
            txtVentaTotal.Text = (total + (total * iva)).ToString();// MessageBox.Show("MONTO\nSub-total: $"+ total.ToString()+ "\nIva: $"+ (iva*total)+ "\ntotal: $"+(total+(total*iva)), "Venta", MessageBoxButton.OK, MessageBoxImage.Information);
            HabilitarCajaventa(true);
            HabilitarNuevaVenta(false);
            BotonEjecutarVEnta(true);
            InabilitarElbotonCalcular(false);
        }

        private void HabilitarNuevaVenta(bool v) {
            VentaNuevaVenta.IsEnabled = v;
        }

        private void BotonEjecutarVEnta(bool v) {
            VentaBtnVenta.IsEnabled = v;
        }

        private void VentaBtnVenta_Click(object sender, RoutedEventArgs e)
        {
           if (string.IsNullOrEmpty(txtVentaPago.Text)) {
                MessageBox.Show("No ha llenado la casilla de 'Forma de pago'", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(txtVentaCliente.Text)) {
                MessageBox.Show("No ha llenado la casilla de 'Nombre de Cliente'", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (string.IsNullOrEmpty(cmbVentaNombreEmpleado.Text))
            {
                MessageBox.Show("No ha seleccionado  el 'Nombre del Empleado'", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            float total = 0;
            foreach (VentaClass2 item in venta)
            {
                total += item.Total;
            }
            /* txtVentaSubtotal.Text = total.ToString();
             txtVentaIva.Text = (iva * total).ToString();
             txtVentaTotal.Text = (total + (total * iva)).ToString();*/

            if (float.Parse(txtVentaPago.Text) < (total + (total * iva))) {
                MessageBox.Show("Forma de pago inferior a la venta", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            float cambio = 0;
            cambio = (float.Parse(txtVentaPago.Text)) - (total + (total * iva));
            MessageBox.Show("MONTO\nSub-total: $" + total.ToString() + "\nIva: $" + (iva * total) + "\ntotal: $" + (total + (total * iva)) + "\nForma de pago: $" + txtVentaPago.Text + "\nCambio: $" + cambio, "Venta", MessageBoxButton.OK, MessageBoxImage.Information);
            
            /*Guardar los datos en el archivo*/
            AccionesArchivo reporte = new AccionesArchivo(txtVentaCliente.Text.ToUpper() + ".poo");
            string datos = "";
            datos = string.Format("Farmacia 'Mi QUERIDO ENFERMITO'\nFecha: {0}\nEmpleado: {1}\nCliente: {2}\n-------------------------------------\nProducto   Precio Cantidad Total\n-------------------------------------\n",  txtVentaFecha.Text, cmbVentaNombreEmpleado.Text.ToUpper(), txtVentaCliente.Text.ToUpper());
            string elementos = "";
            foreach (VentaClass2 item in venta)
            {
                elementos += string.Format("{0}      {1}     {2}     {3}\n", item.Producto, item.Precio, item.Cantidad, item.Total);
            }
            string informacion = string.Format("\nSubtotal: ${0}\nIva: ${1}\nTotal: ${2}\nForma de pago: ${3}\nCambio: ${4}\n\n   ¡¡¡Vuelva pronto!!!", txtVentaSubtotal.Text, txtVentaIva.Text, txtVentaTotal.Text, txtVentaPago.Text, cambio);
            reporte.Guardar(datos + elementos + informacion);
            MessageBox.Show("Reporte Guardado con Exito: " + txtVentaCliente.Text.ToUpper() + ".poo", "Reporte", MessageBoxButton.OK, MessageBoxImage.Information);
            /*Guardar los datos en el archivo*/
            /*LLenar los datos del Inventario*/
            try
            {
                InventarioVentas Ventas = new InventarioVentas()
                {
                    Nombre_Cliente = txtVentaCliente.Text.ToUpper(),
                    Cambio = cambio,
                    Fecha = DateTime.Now,
                    Iva = float.Parse(txtVentaIva.Text),
                    Nombre_Empleado = cmbVentaNombreEmpleado.Text,
                    Subtotal = float.Parse(txtVentaSubtotal.Text),
                    Total = total+(total*iva),
                    Forma_Pago = float.Parse(txtVentaPago.Text),
                    Productos_Venta = venta,
                };
                manejadorVenta.Agregar(Ventas);
                AccionesAlmacenDeVentanas();
            }
            catch (Exception)
            {

                MessageBox.Show("No se pudo generar Lista en Inventario de Ventas", "Ventas", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }            
            /*Fin de LLenar Los datos delInvenatrio*/
            InabilitarCamposVenta(false);
            venta = new List<VentaClass2>();//limpia la lista
            dtgListaDeVenta.ItemsSource = null;//limpia la tabla
            ActualizarTablaProductosEnVenta();
            InabilitarBotonVentaVenta(true);
        }



        private void HabilitarCajaventa(bool v) {
            VentaBtnVenta.IsEnabled = v;
            VentaNuevaVenta.IsEnabled = v;
        }

        private void VentaBtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            if ((MessageBox.Show("Esta realmente seguro de cancelar el producto", "Venta", MessageBoxButton.YesNo, MessageBoxImage.Question)) == MessageBoxResult.Yes)
            {
                txtVentaCantidadProducto.Clear();
                //txtVentaNombreProducto.Clear();
            }
        }

        private void InabilitarCamposVenta(bool v)
        {
            txtVentaCantidadProducto.IsEnabled = v;
            txtVentaCantidadProducto.Clear();
            txtVentaCliente.IsEnabled = v;
            txtVentaCliente.Clear();
          //  txtVentaFolio.IsEnabled = v;
            //txtVentaFolio.Clear();
            txtVentaIva.IsEnabled = v;
            txtVentaIva.Clear();
            txtVentaPago.IsEnabled = v;
            txtVentaPago.Clear();
            txtVentaSubtotal.IsEnabled = v;
            txtVentaSubtotal.Clear();
            txtVentaTotal.IsEnabled = v;
            txtVentaTotal.Clear();
            cmbVentaNombreEmpleado.IsEnabled = v;
            //txtVentaNombreProducto.IsEnabled = v;
            VentaBtnAgregar.IsEnabled = v;
            VentaBtnCalcular.IsEnabled = v;
            VentaBtnCancelar.IsEnabled = v;
            VentaBtnVenta.IsEnabled = v;
            VentaBrnCancelarVenta.IsEnabled = v;
            //VentaBtnBuscarProducto.IsEnabled = v;
            VentaBtnEditarProducto.IsEnabled = v;
            VentaBtnEliminarProducto.IsEnabled = v;
            txtVentaFecha.IsEnabled = v;
        }

        private void VentaBrnCancelarVenta_Click(object sender, RoutedEventArgs e)
        {
            if ((MessageBox.Show("Esta realmente seguro de cancelar toda la operación?\nSe descartara toda la venta", "Venta", MessageBoxButton.YesNo, MessageBoxImage.Question)) == MessageBoxResult.Yes)
            {/*Para Actualizar el stock de la venta*/
                if (venta.Count>0) {
                    foreach (VentaClass2 item in venta)
                    {
                        foreach (var dos in manejadorProductos.Lista)
                        {
                            
                            if (item.Producto==dos.Nombre) {
                                dos.Nombre = dos.Nombre;
                                dos.PrecioCompra = dos.PrecioCompra;
                                dos.PrecioVenta = dos.PrecioVenta;
                                dos.Presentacion = dos.Presentacion;
                                dos.Stock = (item.Cantidad + (int.Parse(dos.Stock))).ToString();
                                dos.ProductoCategoria = dos.ProductoCategoria;
                                manejadorProductos.Modificar(dos);
                                ActualizarTablaVentaAlmacen();
                                ActualizarTablaProductos();
                            }
                        }                        
                    }

                }/*Fin de  la actualizacion del stock*/
                InabilitarCamposVenta(false);
                venta = new List<VentaClass2>();//limpia la lista
                dtgListaDeVenta.ItemsSource = null;//limpia la tabla
                ActualizarTablaProductosEnVenta();
                InabilitarBotonVentaVenta(true);
            }
        }

        private void InabilitarBotonVentaVenta(bool v) {
            VentaNuevaVenta.IsEnabled = v;
        }

        private void InabilitarElbotonCalcular(bool v) {
            VentaBtnCalcular.IsEnabled = v;
        }


        private void VentaNuevaVenta_Click(object sender, RoutedEventArgs e)
        {
            InabilitarCamposVenta(true);
            HabilitarCajaventa(true);
            venta = new List<VentaClass2>();//limpia la lista
            dtgListaDeVenta.ItemsSource = null;//limpia la tabla
            ActualizarTablaProductosEnVenta();
            InabilitarBotonVentaVenta(false);
            BotonEjecutarVEnta(false);
        }

        private void VentaBtnEliminarProducto_Click(object sender, RoutedEventArgs e)
        {
            if (dtgListaDeVenta.SelectedItem == null)
            {
                MessageBox.Show("No ha seleccionado ningun elemento de la Lista de productos!!!\nSeleccione la fila a eliminar.", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                if (MessageBox.Show("Realmente esta seguro de eliminar este producto de su lista", "Venta", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) {
                    VentaClass2 p = dtgListaDeVenta.SelectedItem as VentaClass2;
                    /*Para actualizar la tabla de los productps igual*/
                    try
                    {
                        foreach (var item in manejadorProductos.Lista)
                        {
                            if (item.Nombre == p.Producto)
                            {
                                item.Nombre = item.Nombre;
                                item.PrecioCompra = item.PrecioCompra;
                                item.PrecioVenta = item.PrecioVenta;
                                item.Presentacion = item.Presentacion;
                                item.Stock = (p.Cantidad + (int.Parse(item.Stock))).ToString();
                                item.ProductoCategoria = item.ProductoCategoria;
                                manejadorProductos.Modificar(item);
                                ActualizarTablaVentaAlmacen();
                                ActualizarTablaProductos();
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("No se puedo Editar correctamente el producto en Almacen", "Almacen", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    /*Fin de la actulizacion*/
                    venta.Remove(p);
                    ActualizarTablaProductosEnVenta();
                    MessageBox.Show("Producto eliminado Correctamente", "Venta", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private void VentaBtnEditarProducto_Click(object sender, RoutedEventArgs e)
        {
            if (venta.Count == 0)
            {
                MessageBox.Show("No cuenta con ningun producto en la lista", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else {
                if (dtgListaDeVenta.SelectedItem != null)
                {
                    VentaClass2 a = dtgListaDeVenta.SelectedItem as VentaClass2;
                    txtVentaCantidadProducto.Text = a.Cantidad.ToString();
                    N = true;
                }
                else {
                    MessageBox.Show("No ha seleccionado ningun elemento", "Venta", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
            }
        }

        private void dtgAlmacenDeVentaInventario_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            InventarioVentas a = dtgAlmacenDeVentaInventario.SelectedItem as InventarioVentas;
            if (a != null) {
                txtAlmacenSub_Total.Text = a.Subtotal.ToString();
                txtAlmacenIva.Text = a.Iva.ToString();
                txtAlmacenTotal.Text = a.Total.ToString();
                txtAlmacenCambio.Text = a.Cambio.ToString();
                txtAlmacenCliente.Text = a.Nombre_Cliente.ToString();
                txtAlmacenEmpleado.Text = a.Nombre_Empleado.ToString();
                txtAlmacenFecha.Text = a.Fecha.ToString();
                txtAlmacenPago.Text = a.Forma_Pago.ToString();
                dtgAlmacenDeVentaObservarVenta.ItemsSource = a.Productos_Venta;
                
            }
            else
            {
                MessageBox.Show("No ha seleccionado ninguna fila", "Inventario", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void btnAlmacenVentaEliminar_Click(object sender, RoutedEventArgs e)
        {
            InventarioVentas a = dtgAlmacenDeVentaInventario.SelectedItem as InventarioVentas;
            if (a != null)
            {
                if (MessageBox.Show("Realmente quiere eliminar la venta", "Almacen de Ventas", MessageBoxButton.YesNo, MessageBoxImage.Question)==MessageBoxResult.Yes) {
                    manejadorVenta.Eliminar(a.Id);
                    AccionesAlmacenDeVentanas();
                }
            }
            else {
                MessageBox.Show("No ha seleccionado ninguna fila", "Inventario", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void LimpiarBotonesEnAlmacen() {
            txtAlmacenCambio.Clear();
            txtAlmacenCliente.Clear();
            txtAlmacenEmpleado.Clear();
            txtAlmacenFecha.Clear();
            txtAlmacenIva.Clear();
            txtAlmacenSub_Total.Clear();
            txtAlmacenTotal.Clear();
            dtgAlmacenDeVentaObservarVenta.ItemsSource = null;
        }

        private void btnAlmacenVentanaLimpiar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarBotonesEnAlmacen();
        }

        
    }
}
